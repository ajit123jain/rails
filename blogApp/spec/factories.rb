FactoryBot.define do
  
  factory :author do
    name "Ajit"
  end

  factory :book do
    name "Ajit"
    one  "One"
    two  "Two"
    three "Three"
  end

  factory :post do
    name "Ajit"
    content  "Nothing"
    author_id "5b42c6f72c1b500a90c73e05"
   end


end