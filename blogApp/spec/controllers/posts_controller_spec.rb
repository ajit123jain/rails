require 'rails_helper'


RSpec.describe PostsController, type: :controller do
  

  let(:valid_attributes) {
    {
    	:name => "Ajit",
      :content => "No content",
      :author_id => "5b42c6f72c1b500a90c73e05"  
    }
  }

  let(:invalid_attributes) {
    {
    	:name => nil
    }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # PostsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      post = Post.create! valid_attributes
      get :index, params: {}
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      post = Post.create! valid_attributes
      get :show, params: {id: post.to_param} 
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, format: :js , xhr: true
      response.should render_template :new
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      post = Post.create! valid_attributes
      get :edit, params: {id: post.to_param}, format: :js , xhr: true
      response.should render_template :edit
    end
  end

  describe "POST #create" do
    context "with valid params" do
      # it "creates a new Post" do
      #   expect {
      #     post :create, params: {post: valid_attributes}
      #   }.to change(Post, :count).by(1)
      #   expect(response).to be_success
      # end
      
      it "create a new Post " do
        expect {
          post :create, params: {post: valid_attributes},xhr: true
         }.to change(Post, :count).by(1)
      end


      it "redirects to the created post" do
        post :create, params: {post: valid_attributes}, format: :js , xhr: true
        expect(response).to render_template(:create)
      end
    end

    # context "with invalid params" do
    #   it "returns a success response (i.e. to display the 'new' template)" do
    #     post :create, params: {post: invalid_attributes},  format: :js , xhr: true
    #     expect(response).to render_template(:new)
    #   end
    # end
      # context "with invalid params " do
      #     it "returns a success response (i.e. to display the 'new' template)" do
      #       expect { post :create, params: {post: invalid_attributes} , xhr: true }.to_not change(Post, :count)
      #     end  
      # end  
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          :name => "Ajit Jain",
          :content => "Abi content Aaya",
          :author_id => "5b42c6f72c1b500a90c73e05"
        }
      }

      # it "updates the requested post" do
      #   post = Post.create! new_attributes
      #   put :update, params: {id: post.to_param, post: new_attributes}
      #   post.reload
      #   skip("Add assertions for updated state")
      # end

      it "redirects to the post" do
        post = Post.create! new_attributes
        put :update, params: {id: post.to_param, post: valid_attributes} ,format: :js , xhr: true
        response.should render_template :update
      end
    end

    context "with invalid params" do 
      let(:invalid_attributes) {
        {
          :name => nil
        }
      }

      # it "returns a success response (i.e. to display the 'edit' template)" do
      #   post = Post.create!(invalid_attributes)
      #   put :update, params: {id: post.to_param, post: invalid_attributes}, xhr: true

      #   it { expect(post).to raise_error(ActionController::UrlGenerationError) }
      # end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested post" do
      post = Post.create! valid_attributes
      expect {
        delete :destroy, params: {id: post.to_param}, session: valid_session
      }.to change(Post, :count).by(-1)
    end

    it "redirects to the posts list" do
      post = Post.create! valid_attributes
      delete :destroy, params: {id: post.to_param}, session: valid_session
      expect(response).to redirect_to(posts_url)
    end
  end

end
