require 'rails_helper'


RSpec.describe AuthorsController, type: :controller do
  

  let(:valid_attributes) {
    {
    	:name => "Hello"
    }
  }

  let(:invalid_attributes) {
    {
    	:name => nil
    }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # AuthorsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      author = Author.create! valid_attributes
      get :index, params: {}
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      author = Author.create! valid_attributes
      get :show, params: {id: author.to_param} 
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, format: :js , xhr: true
      response.should render_template :new
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      author = Author.create! valid_attributes
      get :edit, params: {id: author.to_param}, format: :js , xhr: true
      response.should render_template :edit
    end
  end

  describe "POST #create" do
    context "with valid params" do
      # it "creates a new Author" do
      #   expect {
      #     post :create, params: {author: valid_attributes}
      #   }.to change(Author, :count).by(1)
      #   expect(response).to be_success
      # end
      
      it "create a new Author " do
        expect {
          post :create, params: {author: valid_attributes},xhr: true
         }.to change(Author, :count).by(1)
      end


      it "redirects to the created author" do
        post :create, params: {author: valid_attributes}, format: :js , xhr: true
        expect(response).to render_template(:create)
      end
    end

    # context "with invalid params" do
    #   it "returns a success response (i.e. to display the 'new' template)" do
    #     post :create, params: {author: invalid_attributes},  format: :js , xhr: true
    #     expect(response).to render_template(:new)
    #   end
    # end
      # context "with invalid params " do
      #     it "returns a success response (i.e. to display the 'new' template)" do
      #       expect { post :create, params: {author: invalid_attributes} , xhr: true }.to_not change(Author, :count)
      #     end  
      # end  
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          :name => "Ajit"
        }
      }

      # it "updates the requested author" do
      #   author = Author.create! new_attributes
      #   put :update, params: {id: author.to_param, author: new_attributes}
      #   author.reload
      #   skip("Add assertions for updated state")
      # end

      it "redirects to the author" do
        author = Author.create! new_attributes
        put :update, params: {id: author.to_param, author: valid_attributes} ,format: :js , xhr: true
        response.should render_template :update
      end
    end

    context "with invalid params" do 
      let(:invalid_attributes) {
        {
          :name => nil
        }
      }

      # it "returns a success response (i.e. to display the 'edit' template)" do
      #   author = Author.create!(invalid_attributes)
      #   put :update, params: {id: author.to_param, author: invalid_attributes}, xhr: true

      #   it { expect(author).to raise_error(ActionController::UrlGenerationError) }
      # end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested author" do
      author = Author.create! valid_attributes
      expect {
        delete :destroy, params: {id: author.to_param}, session: valid_session
      }.to change(Author, :count).by(-1)
    end

    it "redirects to the authors list" do
      author = Author.create! valid_attributes
      delete :destroy, params: {id: author.to_param}, session: valid_session
      expect(response).to redirect_to(authors_url)
    end
  end

end
