require 'rails_helper'

RSpec.describe Author, type: :model do
 # pending "add some examples to (or delete) #{__FILE__}"
  before(:all) do
    @author = create(:author)
  end


  it "is valid with valid attributes" do
  	author = build(:author, name: nil )
 	expect(author).to_not be_valid
  end

  it "is not valid without a name" do
  	#author = Author.new(name: "Ajit")
 	expect(@author).to be_valid
  end
  
  # it "validation for existence of field one " do
  # 	 author = Author.new(name: "Ajit")
  # 	 expect(author.attributes).to include(:name,:_id)
  # 	end

  it "to check record is saving or not" do
  	before_count = Author.count
	  post :create, @author
	  expect(Author.count).not_to eq(before_count)
  end	

end
