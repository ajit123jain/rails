require 'rails_helper'

RSpec.describe Post, type: :model do
 # pending "add some examples to (or delete) #{__FILE__}"
  before(:all) do
    @post = create(:post)
  end


  it "is valid with valid attributes" do
  	post = build(:post, name: nil )
 	expect(post).to_not be_valid
  end

  it "is not valid without a name" do
  	#post = Post.new(name: "Ajit")
 	expect(@post).to be_valid
  end
  
  # it "validation for existence of field one " do
  # 	 post = Post.new(name: "Ajit")
  # 	 expect(post.attributes).to include(:name,:_id)
  # 	end

  # it "to check record is saving or not" do
  # 	before_count = Post.count
	 #  post :create, @post
	 #  expect(Post.count).not_to eq(before_count)
  # end	

end
