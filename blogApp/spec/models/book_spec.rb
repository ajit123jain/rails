require 'rails_helper'

RSpec.describe Book, type: :model do
 # pending "add some examples to (or delete) #{__FILE__}"
  it "is valid with valid attributes" do
  	#book = Book.new(name: nil)
 	expect(book).to_not be_valid
  end
  it "is not valid without a name" do
  	book = Book.new(name: "Ajit")
 	expect(book).to be_valid
  end
  
  it "validation for existence of field one " do
  	 book = Book.new(name: "Ajit")
  	 expect(book.attributes).to include(:name,:_id)
  	end

  it "to check record is saving or not" do
  	before_count = Book.count
	post :create, :book => {:book => :attributes }
	expect(Book.count).not_to eq(before_count)
  end	

  it "is not valid without a start_date"
  it "is not valid without a end_date"


end
