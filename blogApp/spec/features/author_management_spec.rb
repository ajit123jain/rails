require "rails_helper"

RSpec.feature "Author management", :type => :feature do
  scenario "User creates new Author " do
    visit "/authors"
    sleep 2
    click_link('new_author')
  	
  	expect(page).to have_text("Name")

  	fill_in "Name", :with => "Ajit Jain"

    click_button "Submit"

  	expect(page).to have_text("Ajit Jain")
  	expect(page).to have_link("Edit")
  	expect(page).to have_link("Delete")
  	expect(page).to have_text("Authors")
    sleep 2
  end

  scenario "User will see the authors list" do
  	visit "/authors"
    sleep 2
  	expect(page).to have_text("Authors")
  	expect(page).to have_link('new_author')
    sleep 2
  end

  scenario "Edit the Author" do
  	visit "/authors"
  	click_link('edit')
  	
  	expect(page).to have_text("Name")

  	fill_in "Name", :with => "Anoop Baju"

    click_button "Submit"

  	expect(page).to have_text("Anoop Baju")
  	expect(page).to have_link("Edit")
  	expect(page).to have_link("Delete")
  	expect(page).to have_text("Authors")

  end	

  scenario "Destroy the Author" do
  	visit "/authors"

  	click_link('destroy')
  	
    # a = page.driver.browser.switch_to.alert
    # expect(a.text).to eq("Are you sure?")
    # a.accept
    expect(page).to have_text("Delete Author")
    expect(page).to have_text("No, Please Don't")
    click_link('delete')
  	expect(page).to have_text("Authors")
  	expect(page).to have_link('new_author')
  end			

end