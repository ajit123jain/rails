require "rails_helper"

RSpec.feature "Book management", :type => :feature do
  scenario "User creates new Book " do
    visit "/books/new"
    sleep 2
    fill_in "Name", :with => "Ramayan"
    expect{
      click_button 'Submit'
    }.to change(Book, :count).by(1)


    expect(page).to have_text("Book was successfully created.")
    expect(page).to have_link('Back')
    sleep 2
  end

  scenario "User will see the books list" do
  	visit "/books"
    sleep 2
  	expect(page).to have_text("Books")
  	expect(page).to have_link('new_book')
    sleep 2
  end

  scenario "Edit the Book" do
  	visit "/books"
  	click_link('edit')
  	
  	expect(page).to have_text("Editing Book")

  	fill_in "Name", :with => "Ramayan"

    click_button "Submit"

  	expect(page).to have_text("Ramayan")
  	expect(page).to have_link("Edit")
  	expect(page).to have_link("Back")
  	expect(page).to have_text("Book was successfully updated.")

  end	

  scenario "Destroy the Book" do
  	visit "/books"

  	click_link('destroy')
  	
    a = page.driver.browser.switch_to.alert
    expect(a.text).to eq("Are you sure?")
    a.accept

  	expect(page).to have_text("Book was successfully destroyed.")
  	expect(page).to have_text("Books")
  	expect(page).to have_link('new_book')
  end			

end