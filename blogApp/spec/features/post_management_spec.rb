require "rails_helper"

RSpec.feature "Post management", :type => :feature do
  scenario "User creates new Post " do
    visit "/posts"
    sleep 2
    click_link('new_post')
  	
  	expect(page).to have_text("New Posts")
    expect(page).to have_text("Picture")

  	fill_in "Name", :with => "Ajit Jain"
    fill_in "Content", :with => "No Content"
    click_button "Submit"

     expect(page).to have_text("Ajit Jain")
     expect(page).to have_text("No Content")
  	 expect(page).to have_link("Edit")
  	 expect(page).to have_link("Delete")
  	 expect(page).to have_text("Posts")
    sleep 2
  end

  scenario "User will see the posts list" do
  	visit "/posts"
    sleep 2
  	expect(page).to have_text("Posts")
  	expect(page).to have_link('new_post')
    sleep 2
  end

  scenario "Edit the Post" do
  	visit "/posts"
  	click_link('edit')
  	
  	expect(page).to have_text("Name")
    expect(page).to have_text("Picture")

  	fill_in "Name", :with => "Anoop Baju"
    fill_in "Content", :with => "Baju ka Content"
 
    click_button "Submit"

  	expect(page).to have_text("Anoop Baju")
    expect(page).to have_text("Baju ka Content")
  	expect(page).to have_link("Edit")
  	expect(page).to have_link("Delete")
  	expect(page).to have_text("Posts")

  end	

  scenario "Destroy the Post" do
  	visit "/posts"

  	click_link('destroy')
  	
    # a = page.driver.browser.switch_to.alert
    # expect(a.text).to eq("Are you sure?")
    # a.accept
    expect(page).to have_text("Delete Post")
    expect(page).to have_text("No, Please Don't")
    click_link('delete')
  	expect(page).to have_text("Posts")
  	expect(page).to have_link('new_post')
  end			

end