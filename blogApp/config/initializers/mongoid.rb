# module BSON
# class ObjectId
#   alias :to_json :to_s
#   alias :as_json :to_s
# end
# end

#to get the id in string and not BSON object
module BSON
class ObjectId
def to_json(*args)
to_s.to_json
end

def as_json(*args)
to_s.as_json
end
end
end