require 'api_constraints'
Rails.application.routes.draw do
  # namespace :api, defaults: {format: 'json'} do
  #   scope module: :v1, constraints: ApiConstraints.new(version: 1) do
  #     resources :posts
  #   end
  #   scope module: :v2, constraints: ApiConstraints.new(version: 2, default: true) do
  #     resources :posts
  #   end
  # end 
  namespace :api do
    namespace :v1 do
      resources :posts
      resources :authors
      resources :books
    end  
  end

  namespace :api do
    namespace :v2 do
      resources :posts
      resources :authors
      #resources :books
    end  
  end

  resources :books
  resources :authors do
    resources :posts
  	get "delete"
  end
  resources :posts do
  	get "delete"
    resources :comments do
    	get "delete"
    	end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
