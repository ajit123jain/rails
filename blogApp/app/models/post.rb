class Post
  include Mongoid::Document

  field :name, type: String
  field :content, type: String
  field :picture, type: String
  
  validates_presence_of :name
  embeds_many :comments
  embeds_many :tags
  accepts_nested_attributes_for :tags , allow_destroy: true
  belongs_to :author
  mount_uploader :picture ,PictureUploader
end
