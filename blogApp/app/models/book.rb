class Book
  include Mongoid::Document
  field :name, type: String
  field :one,  type: String
  field :two,  type: String
  field :three, type: String
  validates_presence_of :name
end

