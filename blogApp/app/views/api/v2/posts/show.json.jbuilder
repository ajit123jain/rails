json.name @post.name
json.content @post.content
json.picture @post.picture
json.id @post.id.to_s
json.auhtor_id @post.author_id.to_s

json.tags @post.tags do |tag|
    json.name tag.name
    json.id tag.id.to_s
  end

  json.comments @post.comments do |comment|
    json.(comment, :name, :content)
  end