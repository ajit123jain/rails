json.array! @posts do |post|	
  #json.extract! post, :name, :content, :picture
  json.name post.name
  json.content post.content
  json.picture post.picture
  json.id post.id
  json.author_id post.author_id

  json.tags post.tags do |tag|
    #json.(tag, :name,:_id)
    json.name tag.name
    json.id tag.id.to_s
  end

  json.comments post.comments do |comment|
    json.(comment, :name, :content)
  end

end