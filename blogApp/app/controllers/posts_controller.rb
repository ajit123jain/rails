class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :all_posts
  # GET /posts
  # GET /posts.json
  def index
    
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
    @post.tags.build
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    # @post.save
    # tags = params[:tags]
    #   tags.each do |tag|
    #   @post.tags.create! :name => tag
    #   end
    respond_to do |format|
      if @post.save
        format.js { }
        # format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    ap params.permit!
    
    respond_to do |format|
      if @post.update(update_post_params)
        format.js { }
        #format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        #format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def delete
     @post = Post.find(params[:post_id])
  end


  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end

   ##  format.js { }
     # format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
     # format.json { head :no_content }
   # end
  end

  private
    def all_posts
      @posts = Post.all
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:name,:content,:author_id,:picture,tags_attributes: [:name,:_destroy])
    end

    def update_post_params
       params.require(:post).permit(:name,:content,:author_id,:picture,tags_attributes: [:name,:_id,:_destroy])   
    end
end
