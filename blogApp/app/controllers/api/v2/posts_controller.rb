module Api
    module V2
        class PostsController < ApplicationController
        	class Api::V2::PostsController < ApplicationController
        		skip_before_action :verify_authenticity_token
        		before_action :set_post, only: [:show, :edit, :update, :destroy]

	        	def index
	        		@posts = Post.order('created_at DESC')
	      		end

	      		def create
				    @post = Post.new(post_params)
				    respond_to do |format|
				      if @post.save
				        format.json { render :show, status: :created, location: @post }
				      else
				        format.json { render json: @post.errors, status: :unprocessable_entity }
				      end
				    end
  				end

  				def update
				    @post.update(update_post_params)
				    respond_to do |format|
				      if @post.save
				        format.json { render :show, status: 200, location: @post }
				      else
				        format.json { render json: @post.errors, status: :unprocessable_entity }
				      end
				    end
  				end

  				def destroy
		         @post.destroy 
		        end	

  				def all_posts
					@posts = Post.all
				end
					# Use callbacks to share common setup or constraints between actions.
					def set_post
					@post = Post.find(params[:id])
					end
					
  				def post_params
		          params.require(:post).permit(:name,:content,:author_id,:picture,tags_attributes: [:name,:_destroy])
		        end

		        def update_post_params
		           params.require(:post).permit(:name,:content,:author_id,:picture,tags_attributes: [:name,:_id,:_destroy])   
		        end
			end	
        end
    end
end