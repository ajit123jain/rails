module Api
  module V1
    class PostsController < ApplicationController
      class Api::V1::PostsController < ApplicationController
        before_action :set_post, only: [:show, :update, :destroy]
        skip_before_action :verify_authenticity_token
        # GET /posts
        def index
         @posts = Post.all
         render json: @posts
        end

        # GET /posts/1
        def show
         render json: @post
        end
        # POST /posts
        def create
         @post = Post.new(post_params)
         if @post.save
          render json: @post, status: :created, location:        api_v1_post_url(@post)
         else
          render json: @post.errors, status: :unprocessable_entity
         end
        end
        # PATCH/PUT /posts/1
        def update
         if @post.update(update_post_params)
          render json: @post
         else
          render json: @post.errors, status: :unprocessable_entity
         end
        end
       # DELETE /posts/1
        def destroy
         @post.destroy
        end
        private
        # Use callbacks to share common setup or constraints between actions.
        def set_post
         @post = Post.find(params[:id])
        end
        # Only allow a trusted parameter “white list” through.
        # Never trust parameters from the scary internet, only allow the white list through.
        def post_params
          params.require(:post).permit(:name,:content,:author_id,:picture,tags_attributes: [:name])
        end

        def update_post_params
           params.require(:post).permit(:name,:content,:author_id,:picture,tags_attributes: [:name,:_id,:_destroy])   
        end
        end
    end
  end
end