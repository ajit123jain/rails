json.extract! actor, :id, :title, :created_at, :updated_at
json.url actor_url(actor, format: :json)
